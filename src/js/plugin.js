minervaDefine(function () {
  return {
    register: function (minervaObject) {
      minervaObject.element.innerHTML = "<div>" +
        "<label>Map: </label><input id='model-id'/><br/>" +
        "<label>Zoom: </label><input id='zoom'/></br>" +
        "<label>P1: </label><input id='left-top-corner'/><br/>" +
        "<label>P2: </label2><input id='right-bottom-corner'/><br/>" +
        "<label>Dialog content pos: </label><input id='dialog-left-top-corner'/><br/>" +
        "<label>Dialog content size: </label><input id='dialog-dimension'/><br/>" +
        "<label>Overlays: </label><input id='overlays-id'/><br/></div>";

      var visibleMaps = [minervaObject.project.data.getModels()[0].modelId];

      //listeners for bounds change
      var persistBoundsData = function (modelId) {
        var bounds = minervaObject.project.map.getBounds({modelId: modelId});
        var zoom = minervaObject.project.map.getZoom({modelId: modelId});
        $("#left-top-corner", minervaObject.element).val(Math.round(bounds.getTopLeft().x) + ";" + Math.round(bounds.getTopLeft().y));
        $("#right-bottom-corner", minervaObject.element).val(Math.round(bounds.getRightBottom().x) + ";" + Math.round(bounds.getRightBottom().y));
        $("#zoom", minervaObject.element).val(Math.round(zoom));
        return minervaObject.pluginData.setUserParam("bounds-" + modelId, Math.round(bounds.getTopLeft().x) + ";" + Math.round(bounds.getTopLeft().y) + " " + Math.round(bounds.getRightBottom().x) + ";" + Math.round(bounds.getRightBottom().y)).then(function () {
          return minervaObject.pluginData.setUserParam("zoom-" + modelId, Math.round(zoom));
        });
      };

      //listener for dialog position/size change
      var persistDialogBoundsData = function (modelId, position, size) {
        $("#dialog-left-top-corner", minervaObject.element).val(Math.round(position.x) + ";" + Math.round(position.y));
        $("#dialog-dimension", minervaObject.element).val(Math.round(size.width) + ";" + Math.round(size.height));
        return minervaObject.pluginData.setUserParam("dialog-position-" + modelId, Math.round(position.x) + ";" + Math.round(position.y)).then(function () {
          return minervaObject.pluginData.setUserParam("dialog-dimension-" + modelId, Math.round(size.width) + ";" + Math.round(size.height));
        });
      };

      //listener for set of visible maps changed
      var persistVisibleMapsData = function () {
        var serializedIds = visibleMaps.join(",");
        $("#model-id", minervaObject.element).val(serializedIds);
        return minervaObject.pluginData.setUserParam("visible-maps", serializedIds);
      };

      minervaObject.project.map.addListener({
        object: "map",
        type: "onZoomChanged",
        callback: function (data) {
          return persistBoundsData(data.modelId);
        }
      });
      minervaObject.project.map.addListener({
        object: "map",
        type: "onCenterChanged",
        callback: function (data) {
          return persistBoundsData(data.modelId);
        }
      });

      minervaObject.project.map.addListener({
        object: "map-dialog",
        type: "onFocus",
        callback: function (data) {
          var index = visibleMaps.indexOf(data.modelId);
          if (index > -1) {
            visibleMaps.splice(index, 1);
          }
          visibleMaps.unshift(data.modelId);
          return persistVisibleMapsData();
        }
      });
      minervaObject.project.map.addListener({
        object: "map-dialog",
        type: "onOpen",
        callback: function (data) {
          return persistDialogBoundsData(data.modelId, data.position, data.size).then(function () {
            return persistBoundsData(data.modelId);
          });
        }
      });
      minervaObject.project.map.addListener({
        object: "map-dialog",
        type: "onClose",
        callback: function (data) {
          var index = visibleMaps.indexOf(data.modelId);
          if (index > -1) {
            visibleMaps.splice(index, 1);
          }
          return persistVisibleMapsData();
        }
      });
      minervaObject.project.map.addListener({
        object: "map-dialog",
        type: "onDrag",
        callback: function (data) {
          return persistDialogBoundsData(data.modelId, data.position, data.size);
        }
      });
      minervaObject.project.map.addListener({
        object: "map-dialog",
        type: "onResize",
        callback: function (data) {
          return persistDialogBoundsData(data.modelId, data.position, data.size);
        }
      });

      //listeners for selected data overlays change
      var persistOverlayData = function () {
        return minervaObject.project.map.getVisibleDataOverlays().then(function (overlays) {
          var ids = "";
          for (var i = 0; i < overlays.length; i++) {
            ids += overlays[i].getId() + ",";
          }
          $("#overlays-id", minervaObject.element).val(ids);
          return minervaObject.pluginData.setUserParam("visible-overlays", ids)
        });
      };
      minervaObject.project.map.addListener({
        object: "overlay",
        type: "onHide",
        callback: function () {
          return persistOverlayData();
        }
      });
      minervaObject.project.map.addListener({
        object: "overlay",
        type: "onShow",
        callback: function () {
          return persistOverlayData();
        }
      });

      //and now setup initial values
      return persistOverlayData().then(function () {
        return persistBoundsData(minervaObject.project.data.getModels()[0].modelId);
      }).then(function () {
        return persistVisibleMapsData();
      });
      
    },
    unregister: function () {
    },
    getName: function () {
      return "vizAR";
    },
    getVersion: function () {
      return "0.0.5";
    }
  };
});


// set background color of LOGO for better positioning of AR kit
$('#LOGO_2_IMG').attr('src', 'https://www.zielonyogrodek.pl/images/media2/33654bialy_kwiat_zlocien_fot._Pezibear_-_Pixabay.com.jpg');
$('#LOGO_2_IMG').width(150);
$('#LOGO_2_IMG').height(150);
$('#LOGO_IMG').attr('src', 'https://www.zielonyogrodek.pl/images/media2/33664rozowa_jezowka_kwiat_fot._pixel2013_-_Pixabay.com.jpg');
$('#LOGO_IMG').width(150);
$('#LOGO_IMG').height(150);

$(".minerva-plugin").hide(); 
